package com.arifaradisa.spctest.util

object AppConstant {

    const val prefName = "spc-pref"
    const val dbName = "spc.db"
    const val dbVersion = 1

}