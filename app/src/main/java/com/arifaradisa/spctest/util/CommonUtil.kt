package com.arifaradisa.spctest.util

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.content.pm.PackageInfoCompat
import java.math.RoundingMode
import java.text.DecimalFormat

object CommonUtil {

    fun roundOffDecimal(number: Double): Double? {
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toDouble()
    }

    fun capitalizeFirstLetter(text: String): String {
        return text.substring(0,1).toUpperCase() + text.substring(1)
    }

    fun getAppVersion(context: Context): Int {
        var version: Int
        try {
            val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            version = PackageInfoCompat.getLongVersionCode(packageInfo).toInt()
        } catch (e: PackageManager.NameNotFoundException) {
            version = 0
            e.printStackTrace()
        }

        return version
    }

    fun getVersionName(context: Context): String {
        var version: String
        try {
            val packageInfo = context.packageManager.getPackageInfo(context.packageName, 0)
            version = packageInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            version = "Unstable"
            e.printStackTrace()
        }

        return version
    }

    fun openApp(context: Context, packageName: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("market://details?id=$packageName")
        context.startActivity(intent)
    }

}