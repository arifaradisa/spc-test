package com.arifaradisa.spctest.util

import android.app.Activity
import androidx.appcompat.app.AlertDialog

object DialogUtil {

    interface OnDialogSingleButtonClickListener {
        fun onButtonPositiveClick()
    }

    interface OnDialogDoubleButtonClickListener {
        fun onButtonNegativeClick()
        fun onButtonPositiveClick()
        fun onButtonCloseClick()
    }

    fun showDialogWarning(
        activity: Activity,
        title: String,
        message: String,
        listener: OnDialogSingleButtonClickListener
    ) {
        val dialog = AlertDialog.Builder(activity)

        dialog.apply {
            setCancelable(false)
            setTitle(title)
            setMessage(message)
        }

        dialog.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss()
            listener.onButtonPositiveClick()
        }

        dialog.show()

    }

}