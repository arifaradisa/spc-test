package com.arifaradisa.spctest.util

import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object NetworkUtil {

    fun handleError(throwable: Throwable): String {
        return when (throwable) {
            is SocketTimeoutException -> "Server didn't respond. Please try again later"
            is ConnectException -> "No internet connection"
            is UnknownHostException -> "No internet connection or server fault"
            is IOException -> "Parse error. Please contact administrator"
            else -> "Something wrong"
        }
    }
}