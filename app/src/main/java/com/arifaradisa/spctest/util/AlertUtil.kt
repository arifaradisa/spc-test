package com.arifaradisa.spctest.util

import android.content.Context
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

object AlertUtil {

    fun showToast(context: Context, message: String) {
        Toast.makeText(context, message,  Toast.LENGTH_SHORT).show()
    }

    fun showSnackbar(view: View, message: String) {
        val snackbar = Snackbar
            .make(view, message, Snackbar.LENGTH_LONG)
        snackbar.show()
    }

}