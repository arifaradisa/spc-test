package com.arifaradisa.spctest

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.arifaradisa.spctest.di.AppInjector
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import timber.log.Timber
import javax.inject.Inject

class SpcApp: Application(), HasAndroidInjector{

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        initializeAppInjector()
        initializeServiceInjector()
        initializeLog()
        initializeTime()
    }

    private fun initializeTime() {
        AndroidThreeTen.init(this)
    }

    private fun initializeAppInjector() {
        AppInjector.init(this)
    }

    private fun initializeServiceInjector() {

    }

    private fun initializeLog() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = activityDispatchingAndroidInjector

}