package com.arifaradisa.spctest.di.builder

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arifaradisa.spctest.di.ViewModelKey
import com.arifaradisa.spctest.internal.ViewModelFactory
import com.arifaradisa.spctest.ui.main.MainViewModel
import com.arifaradisa.spctest.ui.main.home.HomeViewModel
import com.arifaradisa.spctest.ui.main.home.list.ListViewModel
import com.arifaradisa.spctest.ui.main.home.pending.PendingViewModel
import com.arifaradisa.spctest.ui.main.profile.ProfileViewModel
import com.arifaradisa.spctest.ui.main.upsert.UpsertViewModel
import com.arifaradisa.spctest.ui.signin.SigninViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelBuilder {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SigninViewModel::class)
    internal abstract fun bindSigninViewModel(signinViewModel: SigninViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    internal abstract fun bindProfileViewModel(profileViewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    internal abstract fun bindListViewModel(listViewModel: ListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PendingViewModel::class)
    internal abstract fun bindPendingViewModel(pendingViewModel: PendingViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UpsertViewModel::class)
    internal abstract fun bindUpsertViewModel(upsertViewModel: UpsertViewModel): ViewModel

}