package com.arifaradisa.spctest.di.builder

import com.arifaradisa.spctest.ui.main.MainActivity
import com.arifaradisa.spctest.ui.main.upsert.UpsertActivity
import com.arifaradisa.spctest.ui.signin.SigninActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindSigninActivity(): SigninActivity

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindUpsertActivity(): UpsertActivity

}