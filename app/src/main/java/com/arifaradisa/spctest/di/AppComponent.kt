package com.arifaradisa.spctest.di

import android.app.Application
import com.arifaradisa.spctest.SpcApp
import com.arifaradisa.spctest.di.builder.ActivityBuilder
import com.arifaradisa.spctest.di.builder.FragmentBuilder
import com.arifaradisa.spctest.di.builder.ServiceBuilder
import com.arifaradisa.spctest.di.module.AppModule
import com.arifaradisa.spctest.di.module.NetModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    NetModule::class,
    ActivityBuilder::class,
    FragmentBuilder::class,
    ServiceBuilder::class
])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder
        fun build(): AppComponent
    }

    fun inject(spcApp: SpcApp)
}