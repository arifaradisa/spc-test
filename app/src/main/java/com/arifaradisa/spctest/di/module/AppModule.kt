package com.arifaradisa.spctest.di.module

import android.app.Application
import androidx.room.Room
import com.arifaradisa.spctest.data.local.db.SpcDatabase
import com.arifaradisa.spctest.data.local.db.dao.KtpDao
import com.arifaradisa.spctest.data.local.pref.PrefHelper
import com.arifaradisa.spctest.di.builder.ViewModelBuilder
import com.arifaradisa.spctest.util.AppConstant
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelBuilder::class])
class AppModule {

    @Provides
    @Singleton
    fun providePreferences(application: Application): PrefHelper {
        return PrefHelper(application)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): SpcDatabase {
        return Room
            .databaseBuilder(app, SpcDatabase::class.java, AppConstant.dbName)
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideKtp(spcDatabase: SpcDatabase): KtpDao {
        return spcDatabase.ktpDao()
    }

}