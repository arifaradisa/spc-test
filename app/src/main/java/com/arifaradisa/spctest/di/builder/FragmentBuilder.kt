package com.arifaradisa.spctest.di.builder

import com.arifaradisa.spctest.ui.main.home.HomeFragment
import com.arifaradisa.spctest.ui.main.home.list.ListFragment
import com.arifaradisa.spctest.ui.main.home.pending.PendingFragment
import com.arifaradisa.spctest.ui.main.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract fun bindHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun bindProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun bindListFragment(): ListFragment

    @ContributesAndroidInjector
    abstract fun bindPendingFragment(): PendingFragment
}