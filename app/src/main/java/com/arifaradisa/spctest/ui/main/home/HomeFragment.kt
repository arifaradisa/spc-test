package com.arifaradisa.spctest.ui.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.arifaradisa.spctest.R
import com.arifaradisa.spctest.ui.base.ScopedFragment
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : ScopedFragment(), HasAndroidInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
        viewModel.let { lifecycle.addObserver(it) }

        bindUI()
    }

    private fun bindUI() {
        initTab()
    }

    private fun initTab() {
        activity?.run {
            val fragmentAdapter = HomePagerAdapter(supportFragmentManager)
            home_viewpager.adapter = fragmentAdapter

            home_tablayout.setupWithViewPager(home_viewpager)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = activityDispatchingAndroidInjector
}
