package com.arifaradisa.spctest.ui.main.home.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.arifaradisa.spctest.R
import com.arifaradisa.spctest.data.network.response.ktp.Ktp
import com.arifaradisa.spctest.ui.base.ScopedFragment
import com.gamamulti.app.helper.EndlessScrollListener
import com.gamamulti.app.helper.ListPaddingDecoration
import com.gamamulti.app.ui.main.home.attendance.ListAdapter
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class ListFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ListViewModel
    lateinit var scrollListener: EndlessScrollListener
    lateinit var layout: LinearLayoutManager
    lateinit var listAdapter: ListAdapter
    private val temp = arrayListOf<Ktp>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ListViewModel::class.java)
        viewModel.let { lifecycle.addObserver(it) }

        initRecycler()
        bindUI()
    }

    private fun bindUI() {
        launch {
            viewModel.getList(1)
        }
        viewModel.ktpList.observe(this@ListFragment, Observer {
            it.data?.let { ktp ->
                listAdapter.showLoaded(false)
                if (ktp.isNotEmpty()) {
                    temp.addAll(ktp)
                    listAdapter.notifyDataSetChanged()
                    if (temp.size<10){
                        listAdapter.showEndOfList()
                    }
                }else{
                    if(temp.isNotEmpty()) {
                        listAdapter.showEndOfList()
                    }else{
                        Timber.d("Kosong")
                    }
                }
            }
        })
    }

    private fun initRecycler() {
        activity?.run {
            layout = LinearLayoutManager(this)
            scrollListener = object : EndlessScrollListener(layout) {
                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                    launch {
                        if ((temp.isNotEmpty())and(temp.size>=10)) {
                            listAdapter.showLoaded(true)
                            viewModel.getList(page)
                        }
                        Timber.d("PAGE = $page")
                    }
                }

            }
            list_recycler.addOnScrollListener(scrollListener)

            listAdapter = ListAdapter(temp as MutableList<Ktp>, this)
            list_recycler.apply {
                layoutManager = layout
                adapter = listAdapter
                addItemDecoration(
                    ListPaddingDecoration(
                        this.context,
                        0,
                        0
                    )
                )
            }
        }

    }

}
