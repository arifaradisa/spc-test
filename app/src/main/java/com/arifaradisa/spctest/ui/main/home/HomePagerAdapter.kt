package com.arifaradisa.spctest.ui.main.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.arifaradisa.spctest.ui.main.home.list.ListFragment
import com.arifaradisa.spctest.ui.main.home.pending.PendingFragment

class HomePagerAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ListFragment()
            else -> PendingFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "List"
            else -> "Pending"
        }
    }
}