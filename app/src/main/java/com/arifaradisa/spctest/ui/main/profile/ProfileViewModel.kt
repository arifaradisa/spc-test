package com.arifaradisa.spctest.ui.main.profile

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arifaradisa.spctest.data.repository.UserRepository
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel(), LifecycleObserver {

    private val _signoutResponse = MutableLiveData<Int>()
    private val _username = MutableLiveData<String>()

    val signoutResponse: LiveData<Int>
        get() =  _signoutResponse
    val username: LiveData<String>
        get() =  _username

    init {
        _username.postValue(userRepository.getUsername())
    }

    suspend fun doSignout() {
        _signoutResponse.postValue(userRepository.doSignout())
    }


}
