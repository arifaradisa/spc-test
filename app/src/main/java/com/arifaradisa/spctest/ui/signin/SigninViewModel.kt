package com.arifaradisa.spctest.ui.signin

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arifaradisa.spctest.data.repository.UserRepository
import javax.inject.Inject

class SigninViewModel @Inject constructor(
    private val userRepository: UserRepository
): ViewModel(), LifecycleObserver {

    val signinResponse = MutableLiveData<Int>()
    val signed = MutableLiveData<Boolean>()

    init {
        signed.postValue(userRepository.isLogged())
    }


    suspend fun doSignin(username: String, password: String) {
        signinResponse.postValue(userRepository.doSignin(username, password))
    }

}