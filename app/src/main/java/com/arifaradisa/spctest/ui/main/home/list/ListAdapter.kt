package com.gamamulti.app.ui.main.home.attendance

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.arifaradisa.spctest.R
import com.arifaradisa.spctest.data.network.response.ktp.Ktp
import com.arifaradisa.spctest.ui.main.home.detail.DetailActivity
import kotlinx.android.synthetic.main.item_list.view.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class ListAdapter(
    private val ktp: MutableList<Ktp>,
    private val context: Context
): RecyclerView.Adapter<ListHolder>() {

    private val VIEW_TYPE_CELL = 0
    private val VIEW_TYPE_FOOTER = 1
    private val VIEW_TYPE_LOADING = 2

    var isLoaded: Boolean = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        return when (viewType) {
            VIEW_TYPE_CELL -> ListHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false), context)
            VIEW_TYPE_LOADING -> ListHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.view_loading_mini, parent, false),
                context
            )
            else -> ListHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.view_end_of_list, parent, false),
                context
            )
        }
    }

    override fun getItemCount(): Int = ktp.size

    override fun onBindViewHolder(holder: ListHolder, position: Int) {
        if (position < ktp.size-1) {
            if (!isLoaded) {
                if (ktp[position].id!=0) {
                    holder.bindList(ktp[position])
                }
            }
        }
        Timber.d("TOTAL = $itemCount")
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == ktp.size-1) {
            if(isLoaded){
                VIEW_TYPE_LOADING
            }else {
                VIEW_TYPE_FOOTER
            }
        } else {
            VIEW_TYPE_CELL
        }

    }

    fun showEndOfList() {
        isLoaded = false
        ktp.add(Ktp(0,"","","","","","", "", "", "", "","","","","","", "", "", "", "", "", "" ))
        notifyDataSetChanged()
    }

    fun showLoaded(value: Boolean) {
        isLoaded = value
    }
}

class ListHolder(view: View, private val context: Context): RecyclerView.ViewHolder(view) {
    private val name = view.list_nama
    private val nik = view.list_nik
    private val keterangan = view.list_keterangan

    fun bindList(
        ktp: Ktp
    ) {
        val dateFormat: Date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(ktp.tglLahir)
        val lahir = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(dateFormat)

        name.text = ktp.nama
        nik.text = ktp.nik
        keterangan.text = ktp.jenisKelamin+"/"+ktp.tmpLahir+", "+lahir

        itemView.setOnClickListener {
            val intent = Intent(context, DetailActivity::class.java).apply {
                putExtra("nik", ktp.nik)
                putExtra("nama", ktp.nama)
                putExtra("tmp", ktp.tmpLahir)
                putExtra("tgl", ktp.tglLahir)
                putExtra("jk", ktp.jenisKelamin)
                putExtra("gd", ktp.golDarah)
                putExtra("alamat", ktp.alamat)
                putExtra("rt", ktp.rt)
                putExtra("rw", ktp.rw)
                putExtra("kel", ktp.kelurahan)
                putExtra("kec", ktp.kecamatan)
                putExtra("kab", ktp.kabupaten)
                putExtra("prov", ktp.provinsi)
                putExtra("agama", ktp.agama)
                putExtra("status", ktp.perkawinan)
                putExtra("kerja", ktp.pekerjaan)
                putExtra("warga", ktp.wargaNegara)
                putExtra("masa", ktp.berlaku)
                putExtra("tmp_kel", ktp.tmpKeluar)
                putExtra("tgl_kel", ktp.tglKeluar)
                putExtra("img_path", ktp.imgPath)
            }
            context.startActivity(intent)
        }
    }

}
