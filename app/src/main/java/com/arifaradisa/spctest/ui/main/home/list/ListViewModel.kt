package com.arifaradisa.spctest.ui.main.home.list

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arifaradisa.spctest.data.network.response.ktp.Ktp
import com.arifaradisa.spctest.data.repository.KtpRepository
import com.arifaradisa.spctest.internal.Resource
import javax.inject.Inject

class ListViewModel @Inject constructor(
    private val ktpRepository: KtpRepository
) : ViewModel(), LifecycleObserver {

    private var _ktpList =  MutableLiveData<Resource<List<Ktp>>>()
    val ktpList: LiveData<Resource<List<Ktp>>>
        get() = _ktpList

    suspend fun getList(page: Int) {
        _ktpList.postValue(ktpRepository.getKtp(page))
    }


}
