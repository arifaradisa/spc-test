package com.arifaradisa.spctest.ui.main.upsert

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arifaradisa.spctest.data.repository.KtpRepository
import com.arifaradisa.spctest.internal.Resource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class UpsertViewModel @Inject constructor(
    private val ktpRepository: KtpRepository
): ViewModel(), LifecycleObserver {

    val addResponse = MutableLiveData<Resource<Int>>()

    suspend fun submitData(
        part: MultipartBody.Part,
        nik: RequestBody,
        nama: RequestBody,
        tmp: RequestBody,
        tgl: RequestBody,
        jk: RequestBody,
        gd: RequestBody,
        almt: RequestBody,
        rt: RequestBody,
        rw: RequestBody,
        kel: RequestBody,
        kec: RequestBody,
        kab: RequestBody,
        prov: RequestBody,
        agama: RequestBody,
        kawin: RequestBody,
        kerja: RequestBody,
        warga: RequestBody,
        masa: RequestBody,
        tmpKl: RequestBody,
        tglKl: RequestBody,
        path: RequestBody
    ) {
        addResponse.postValue(ktpRepository.add(part,nik,nama,tmp,tgl,jk,gd,almt,rt,rw,kel,kec,kab,prov,agama,
            kawin,kerja,warga,masa,tmpKl,tglKl,path))
    }
}