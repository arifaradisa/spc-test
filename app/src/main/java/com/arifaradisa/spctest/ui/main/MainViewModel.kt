package com.arifaradisa.spctest.ui.main

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import com.arifaradisa.spctest.data.repository.UserRepository
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val userRepository: UserRepository
): ViewModel(), LifecycleObserver {

}