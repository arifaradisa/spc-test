package com.arifaradisa.spctest.ui.main.upsert

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.arifaradisa.spctest.R
import com.arifaradisa.spctest.internal.Status
import com.arifaradisa.spctest.ui.base.ScopedActivity
import com.arifaradisa.spctest.util.DialogUtil
import com.gamamulti.app.util.FileUtil
import com.github.florent37.runtimepermission.kotlin.askPermission
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_upsert.*
import kotlinx.coroutines.launch
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import timber.log.Timber
import javax.inject.Inject

class UpsertActivity : ScopedActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: UpsertViewModel

    var jenis= arrayOf("Jenis Kelamin", "Pria", "Wanita")
    var goldar= arrayOf("Golongan Darah", "A", "B", "AB", "O")
    var agama= arrayOf("Agama", "Islam", "Katolik", "Protestan", "Hindu", "Buddha")
    var status= arrayOf("Status Pernikahan", "Belum Kawin", "Kawin")
    var warga= arrayOf("Kewarganegaraan", "WNI", "WNA")

    var nik: String = ""
    var nama: String = ""
    var tmp_lhr: String = ""
    var tgl_lhr: String = ""
    var jekel: String = ""
    var golongan: String = ""
    var alamat: String = ""
    var rt: String = ""
    var rw: String = ""
    var kel: String = ""
    var kec: String = ""
    var kab: String = ""
    var prov: String = ""
    var agam: String = ""
    var kwn: String = ""
    var krja: String = ""
    var wnga: String = ""
    var masa: String = ""
    var tmp_kel: String = ""
    var tgl_kel: String = ""

    lateinit var output: Uri

    private val TAKE_CAMERA_REQUEST_CODE = 2


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upsert)
        AndroidInjection.inject(this)
        initToolbar(intent.hasExtra("update"))
        initViewModel()
        initSpinner()
    }

    private fun initSpinner() {
        val spinner_jenis = findViewById<Spinner>(R.id.upsert_jenis)
        if (spinner_jenis != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, jenis)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_jenis.adapter = arrayAdapter
            spinner_jenis.setSelection(0, false)
            spinner_jenis.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    jekel = jenis[position]
                    Timber.d(jenis[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        val spinner_goldar = findViewById<Spinner>(R.id.upsert_goldar)
        if (spinner_goldar != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, goldar)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_goldar.adapter = arrayAdapter
            spinner_goldar.setSelection(0, false)
            spinner_goldar.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    golongan = goldar[position]
                    Timber.d(goldar[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        val spinner_agama = findViewById<Spinner>(R.id.upsert_agama)
        if (spinner_agama != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, agama)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_agama.adapter = arrayAdapter
            spinner_agama.setSelection(0, false)
            spinner_agama.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    agam = agama[position]
                    Timber.d(agama[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        val spinner_kawin = findViewById<Spinner>(R.id.upsert_kawin)
        if (spinner_kawin != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, status)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_kawin.adapter = arrayAdapter
            spinner_kawin.setSelection(0, false)
            spinner_kawin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    kwn = status[position]
                    Timber.d(status[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }

        val spinner_warga= findViewById<Spinner>(R.id.upsert_warga)
        if (spinner_warga != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, warga)
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner_warga.adapter = arrayAdapter
            spinner_warga.setSelection(0, false)
            spinner_warga.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    wnga = warga[position]
                    Timber.d(warga[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }
    }

    private fun initToolbar(isUpdate: Boolean) {
        setSupportActionBar(upsert_toolbar as Toolbar)
        val titlebar = if (isUpdate) "Update KTP" else "Add KTP"
        supportActionBar?.apply {
            if (isUpdate) {
                title = titlebar
            }
            setHomeAsUpIndicator(ContextCompat.getDrawable(this@UpsertActivity, R.drawable.ic_close_white_24dp))
            setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UpsertViewModel::class.java)
        viewModel.let { lifecycle.addObserver(it) }

        bindUI()
    }

    private fun bindUI() {
        upsert_btn.setOnClickListener {
            if (upsert_nik.text.toString()==""
                || upsert_nama.text.toString()==""
                || upsert_tempat.text.toString()==""
                || upsert_tgl.text.toString()==""
                || jekel == ""
                || golongan == ""
                || upsert_alamat.text.toString() == ""
                || upsert_rt.text.toString()==""
                || upsert_rw.text.toString()==""
                || upsert_kel.text.toString()==""
                || upsert_kec.text.toString()==""
                || upsert_kab.text.toString()==""
                || upsert_prov.text.toString()==""
                || agam == ""
                || kwn == ""
                || upsert_kerja.text.toString() == ""
                || wnga == ""
                || upsert_masa.text.toString()==""
                || upsert_tmp_kel.text.toString()==""
                || upsert_tgl_kel.text.toString()==""

                ){
                DialogUtil.showDialogWarning(
                    this,
                    "Warning",
                    "Please fill all fields",
                    object : DialogUtil.OnDialogSingleButtonClickListener {
                        override fun onButtonPositiveClick() {

                        }
                    }
                )
            }else{
                addPhoto()
            }
        }

        viewModel.addResponse.observe(this, Observer {
            when(it.status){
                Status.SUCCESS -> {
                    Timber.d("success")
                    finish()
                }
                else -> {
                    Timber.d("Something wrong")
                }
            }
        })
    }

    private fun addPhoto() {
        askPermission(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE){ _ ->
            StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder().build())

            output = FileUtil.makeFile(this, upsert_nik.text.toString())
            val iCamera = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            iCamera.putExtra(MediaStore.EXTRA_OUTPUT, output)
            startActivityForResult(iCamera, TAKE_CAMERA_REQUEST_CODE)

        }.onDeclined { e ->
            DialogUtil.showDialogWarning(
                this,
                "Warning",
                "This feature need your permissions",
                object : DialogUtil.OnDialogSingleButtonClickListener {
                    override fun onButtonPositiveClick() {

                    }
                }
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            TAKE_CAMERA_REQUEST_CODE -> {
                when(resultCode){
                    Activity.RESULT_OK -> {
                        val file = FileUtil.compressImage(this, output.toString())

                        val reqBody = RequestBody.create(MediaType.parse("image/*"), file)
                        val part = MultipartBody.Part.createFormData("image", file, reqBody)

                        val nik = RequestBody.create(MediaType.parse("text/plain"), upsert_nik.text.toString())
                        val nama = RequestBody.create(MediaType.parse("text/plain"), upsert_nama.text.toString())
                        val tmp = RequestBody.create(MediaType.parse("text/plain"), upsert_tempat.text.toString())
                        val tgl = RequestBody.create(MediaType.parse("text/plain"), upsert_tgl.text.toString())
                        val jk = RequestBody.create(MediaType.parse("text/plain"), jekel)
                        val gd = RequestBody.create(MediaType.parse("text/plain"), golongan)
                        val almt = RequestBody.create(MediaType.parse("text/plain"), upsert_alamat.text.toString())
                        val rt = RequestBody.create(MediaType.parse("text/plain"), upsert_rt.text.toString())
                        val rw = RequestBody.create(MediaType.parse("text/plain"), upsert_rw.text.toString())
                        val kel = RequestBody.create(MediaType.parse("text/plain"), upsert_kel.text.toString())
                        val kec = RequestBody.create(MediaType.parse("text/plain"), upsert_kec.text.toString())
                        val kab = RequestBody.create(MediaType.parse("text/plain"), upsert_kab.text.toString())
                        val prov = RequestBody.create(MediaType.parse("text/plain"), upsert_prov.text.toString())
                        val agama = RequestBody.create(MediaType.parse("text/plain"), agam)
                        val kawin = RequestBody.create(MediaType.parse("text/plain"), kwn)
                        val kerja = RequestBody.create(MediaType.parse("text/plain"), upsert_kerja.text.toString())
                        val warga = RequestBody.create(MediaType.parse("text/plain"), wnga)
                        val masa = RequestBody.create(MediaType.parse("text/plain"), upsert_masa.text.toString())
                        val tmp_kl = RequestBody.create(MediaType.parse("text/plain"), upsert_tmp_kel.text.toString())
                        val tgl_kl = RequestBody.create(MediaType.parse("text/plain"), upsert_tgl_kel.text.toString())
                        val filename = RequestBody.create(MediaType.parse("text/plain"), "/uploads/"+upsert_nik.text.toString()+".jpg")

                        launch {
                            viewModel.submitData(
                                part,
                                nik,
                                nama,
                                tmp,
                                tgl,
                                jk,
                                gd,
                                almt,
                                rt,
                                rw,
                                kel,
                                kec,
                                kab,
                                prov,
                                agama,
                                kawin,
                                kerja,
                                warga,
                                masa,
                                tmp_kl,
                                tgl_kl,
                                filename
                            )
                        }
                    }
                }
            }
        }

    }

}
