package com.arifaradisa.spctest.ui.signin

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.arifaradisa.spctest.R
import com.arifaradisa.spctest.ui.base.ScopedActivity
import com.arifaradisa.spctest.ui.main.MainActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_signin.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class SigninActivity : ScopedActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: SigninViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        AndroidInjection.inject(this)
        initViewModel()
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SigninViewModel::class.java)
        viewModel.let { lifecycle.addObserver(it) }

        bindUI()
    }

    private fun bindUI() {
        viewModel.signinResponse.observe(this@SigninActivity, Observer {
            when(it){
                200 -> {
                    Timber.d("Login success")
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
                401 -> {
                    Timber.d("Invalid username/password")
                }
                else -> {
                    Timber.d("Something wrong")
                }
            }
        })

        signin_submit.setOnClickListener {
            launch {
                viewModel.doSignin(signin_username.text.toString(), signin_password.text.toString())
            }
        }

        viewModel.signed.observe(this@SigninActivity, Observer {
            if (it){
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        })
    }
}
