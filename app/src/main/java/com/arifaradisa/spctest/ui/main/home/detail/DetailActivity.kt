package com.arifaradisa.spctest.ui.main.home.detail

import android.content.Intent
import android.os.Bundle
import com.arifaradisa.spctest.BuildConfig
import com.arifaradisa.spctest.R
import com.arifaradisa.spctest.ui.base.ScopedActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : ScopedActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        readBundle(intent)
    }

    private fun readBundle(intent: Intent) {
        detail_nik.text = intent.getStringExtra("nik")
        detail_nama.text = intent.getStringExtra("nama")
        detail_ttl.text = intent.getStringExtra("tmp")+"/"+intent.getStringExtra("tgl")
        detail_jk.text = intent.getStringExtra("jk")
        detail_goldar.text = intent.getStringExtra("gd")
        detail_alamat.text = intent.getStringExtra("alamat")
        detail_rtrw.text = intent.getStringExtra("rt")+"/"+intent.getStringExtra("rw")
        detail_kel.text = intent.getStringExtra("kel")
        detail_kec.text = intent.getStringExtra("kec")
        detail_kab.text = intent.getStringExtra("kab")
        detail_prov.text = intent.getStringExtra("prov")
        detail_agama.text = intent.getStringExtra("agama")
        detail_status.text = intent.getStringExtra("status")
        detail_kerja.text = intent.getStringExtra("kerja")
        detail_warga.text = intent.getStringExtra("warga")
        detail_masa.text = intent.getStringExtra("masa")
        detail_tmp_kel.text = intent.getStringExtra("tmp_kel")
        detail_tgl_kel.text = intent.getStringExtra("tgl_kel")

        Glide.with(this)
            .load(BuildConfig.BASE_URL+intent.getStringExtra("img_path"))
            .into(detail_image)
    }
}
