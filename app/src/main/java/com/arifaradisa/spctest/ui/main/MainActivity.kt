package com.arifaradisa.spctest.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.arifaradisa.spctest.R
import com.arifaradisa.spctest.ui.base.ScopedActivity
import com.arifaradisa.spctest.ui.main.home.list.ListFragment
import com.arifaradisa.spctest.ui.main.profile.ProfileFragment
import com.arifaradisa.spctest.ui.main.upsert.UpsertActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : ScopedActivity(), HasAndroidInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: MainViewModel

    private val listFragment: Fragment = ListFragment()
    private val profileFragment: Fragment = ProfileFragment()
    private val fm: FragmentManager = supportFragmentManager
    private var active = listFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AndroidInjection.inject(this)
        initToolbar()
        initViewModel()
        initNavigation()
        initFab()
    }

    private fun initFab() {
        main_fab.setOnClickListener {
            val iUpsert = Intent(this, UpsertActivity::class.java).apply {
                putExtra("update", false)
            }
            startActivity(iUpsert)
        }
    }

    private fun initNavigation() {
        fm.beginTransaction().add(R.id.main_container, profileFragment, "profile").hide(profileFragment).commit();
        fm.beginTransaction().add(R.id.main_container,listFragment, "home").commit();

        main_navigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    private val onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.menu_home -> {
                    fm.beginTransaction().hide(active).show(listFragment).commit();
                    active = listFragment
                    return@OnNavigationItemSelectedListener true
                }
                R.id.menu_profile-> {
                    fm.beginTransaction().hide(active).show(profileFragment).commit();
                    active = profileFragment
                    return@OnNavigationItemSelectedListener true
                }
            }
            return@OnNavigationItemSelectedListener false
        }

    private fun initToolbar() {
        setSupportActionBar(main_toolbar as Toolbar)
        supportActionBar?.apply {
            title = "KTP Indonesia"
        }
    }

    private fun initViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        viewModel.let { lifecycle.addObserver(it) }
    }

    override fun androidInjector(): AndroidInjector<Any> = activityDispatchingAndroidInjector
}
