package com.arifaradisa.spctest.ui.main.home.list

import androidx.paging.PageKeyedDataSource
import com.arifaradisa.spctest.data.local.pref.PrefHelper
import com.arifaradisa.spctest.data.network.RestService
import com.arifaradisa.spctest.data.network.response.ktp.Ktp
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber

class ListDataSource (
    private val restService: RestService,
    private val prefHelper: PrefHelper
) : PageKeyedDataSource<Int, Ktp>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Ktp>
    ) {
        GlobalScope.launch {
            try {
                val response = restService.getList(prefHelper.getAccessToken(),1)
                when{
                    response.isSuccessful -> {
                        val listing = response.body()?.result
                        val ktpData = listing?.ktp?.map { it }
                        callback.onResult(ktpData ?: listOf(), null, 2)
                    }
                }

            }catch (exception : Exception){
                Timber.e("Failed to fetch data!")
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Ktp>) {
        GlobalScope.launch {
            try {
                val response = restService.getList(prefHelper.getAccessToken(),params.key)
                when{
                    response.isSuccessful -> {
                        val listing = response.body()?.result
                        val ktpData = listing?.ktp?.map { it }
                        callback.onResult(ktpData ?: listOf(), params.key+1)
                    }
                }

            }catch (exception : Exception){
                Timber.e("Failed to fetch data!")
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Ktp>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}