package com.arifaradisa.spctest.ui.main.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.arifaradisa.spctest.R
import com.arifaradisa.spctest.ui.base.ScopedFragment
import com.arifaradisa.spctest.ui.signin.SigninActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class ProfileFragment : ScopedFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ProfileViewModel::class.java)
        viewModel.let { lifecycle.addObserver(it) }

        bindUI()
    }

    private fun bindUI() {
        profile_logout.setOnClickListener {
            launch {
                viewModel.doSignout()
            }
        }

        viewModel.username.observe(this@ProfileFragment, Observer {
            profile_username.text = it
        })

        viewModel.signoutResponse.observe(this@ProfileFragment, Observer {
            when(it){
                200 -> {
                    Timber.d("Logout success")
                    activity?.apply{
                        startActivity(Intent(this, SigninActivity::class.java))
                        finish()
                    }
                }
                else -> {
                    Timber.d("Something wrong")
                }
            }
        })
    }

}
