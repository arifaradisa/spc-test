package com.arifaradisa.spctest.data.repository

import com.arifaradisa.spctest.data.local.pref.PrefHelper
import com.arifaradisa.spctest.data.network.RestService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val restService: RestService,
    private val prefHelper: PrefHelper
) {

    suspend fun doSignin(username: String, password: String): Int? {
        return withContext(Dispatchers.IO){
            val signin = restService.doSignin(username, password)
            if (signin.isSuccessful) {
                signin.body()?.let {
                    if (it.status){
                        prefHelper.setAccessToken(it.result.accessToken)
                        prefHelper.setUsername(it.result.username)
                        prefHelper.setLogged(true)
                    }
                    return@withContext it.statusCode
                }
            }else{
                return@withContext 500
            }
        }
    }

    suspend fun doSignout(): Int? {
        return withContext(Dispatchers.IO){
            val signout = restService.doSignout(prefHelper.getAccessToken())
            if (signout.isSuccessful){
                signout.body()?.let {
                    if (it.status) {
                        prefHelper.clearData()
                    }
                    return@withContext it.statusCode
                }
            }else{
                return@withContext 500
            }
        }
    }

    fun getUsername(): String? {
        return prefHelper.getUsername()
    }

    fun isLogged(): Boolean? {
        return prefHelper.isLogged()
    }

}