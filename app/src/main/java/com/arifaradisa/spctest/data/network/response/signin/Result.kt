package com.arifaradisa.spctest.data.network.response.signin


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("accessToken")
    val accessToken: String,
    @SerializedName("username")
    val username: String
)