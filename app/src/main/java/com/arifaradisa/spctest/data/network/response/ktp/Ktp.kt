package com.arifaradisa.spctest.data.network.response.ktp


import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "ktp", indices = [Index(value = ["nik"], unique = true)])
data class Ktp(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("id")
    val id: Int,
    @SerializedName("nik")
    val nik: String,
    @SerializedName("nama")
    val nama: String,
    @SerializedName("tmp_lahir")
    val tmpLahir: String,
    @SerializedName("tgl_lahir")
    val tglLahir: String,
    @SerializedName("jenis_kelamin")
    val jenisKelamin: String,
    @SerializedName("gol_darah")
    val golDarah: String,
    @SerializedName("alamat")
    val alamat: String,
    @SerializedName("rt")
    val rt: String,
    @SerializedName("rw")
    val rw: String,
    @SerializedName("kelurahan")
    val kelurahan: String,
    @SerializedName("kecamatan")
    val kecamatan: String,
    @SerializedName("kabupaten")
    val kabupaten: String,
    @SerializedName("provinsi")
    val provinsi: String,
    @SerializedName("agama")
    val agama: String,
    @SerializedName("perkawinan")
    val perkawinan: String,
    @SerializedName("pekerjaan")
    val pekerjaan: String,
    @SerializedName("warga_negara")
    val wargaNegara: String,
    @SerializedName("berlaku")
    val berlaku: String,
    @SerializedName("tmp_keluar")
    val tmpKeluar: String,
    @SerializedName("tgl_keluar")
    val tglKeluar: String,
    @SerializedName("img_path")
    val imgPath: String
)