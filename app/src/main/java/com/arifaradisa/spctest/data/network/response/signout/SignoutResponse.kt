package com.arifaradisa.spctest.data.network.response.signout


import com.google.gson.annotations.SerializedName

data class SignoutResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("status_code")
    val statusCode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("result")
    val result: Result
)