package com.arifaradisa.spctest.data.repository

import com.arifaradisa.spctest.data.local.db.dao.KtpDao
import com.arifaradisa.spctest.data.local.pref.PrefHelper
import com.arifaradisa.spctest.data.network.RestService
import com.arifaradisa.spctest.data.network.response.ktp.Ktp
import com.arifaradisa.spctest.internal.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class KtpRepository @Inject constructor(
    private val restService: RestService,
    private val prefHelper: PrefHelper,
    private val ktpDao: KtpDao
) {

    suspend fun getKtp(page: Int): Resource<List<Ktp>>? {
        return withContext(Dispatchers.IO){
            val list = restService.getList(prefHelper.getAccessToken(), page)
            if (list.isSuccessful) {
                list.body()?.let {
                    if (it.status){
                        return@withContext Resource.success(it.result.ktp)
                    }else{
                        return@withContext Resource.error(it.message, null)
                    }
                }
            }else{
                return@withContext Resource.error("Something wrong", null)
            }
        }
    }

    suspend fun add(
        part: MultipartBody.Part,
        nik: RequestBody,
        nama: RequestBody,
        tmp: RequestBody,
        tgl: RequestBody,
        jk: RequestBody,
        gd: RequestBody,
        almt: RequestBody,
        rt: RequestBody,
        rw: RequestBody,
        kel: RequestBody,
        kec: RequestBody,
        kab: RequestBody,
        prov: RequestBody,
        agama: RequestBody,
        kawin: RequestBody,
        kerja: RequestBody,
        warga: RequestBody,
        masa: RequestBody,
        tmpKl: RequestBody,
        tglKl: RequestBody,
        path: RequestBody
    ): Resource<Int>? {
        return withContext(Dispatchers.IO){
            val add = restService.addKtp(prefHelper.getAccessToken(),part,nik,nama,tmp,tgl,jk,gd,almt,rt,rw,kel,kec,kab,prov,agama,
                kawin,kerja,warga,masa,tmpKl,tglKl, path)
            if (add.isSuccessful) {
                add.body()?.let {
                    if (it.status){
                        return@withContext Resource.success(it.statusCode)
                    }else{
                        return@withContext Resource.error(it.message, null)
                    }
                }
            }else{
                return@withContext Resource.error("Something wrong", null)
            }
        }
    }


}