package com.arifaradisa.spctest.data.network.response.ktp


import com.google.gson.annotations.SerializedName

data class ListResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("status_code")
    val statusCode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("result")
    val result: Result
)