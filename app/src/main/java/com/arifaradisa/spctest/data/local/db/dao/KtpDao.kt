package com.arifaradisa.spctest.data.local.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.arifaradisa.spctest.data.network.response.ktp.Ktp

@Dao
interface KtpDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ktp: Ktp)

    @Query("DELETE FROM ktp where nik = :nik")
    fun removeLocal(nik: Int)

    @Query("select * from ktp")
    fun getLocal(): LiveData<List<Ktp>>

}