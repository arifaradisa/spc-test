package com.arifaradisa.spctest.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.arifaradisa.spctest.data.local.db.dao.KtpDao
import com.arifaradisa.spctest.data.network.response.ktp.Ktp
import com.arifaradisa.spctest.util.AppConstant

@Database(
    entities = [ Ktp::class ],
    version = AppConstant.dbVersion,
    exportSchema = false
)
abstract class SpcDatabase: RoomDatabase() {

    abstract fun ktpDao(): KtpDao

}