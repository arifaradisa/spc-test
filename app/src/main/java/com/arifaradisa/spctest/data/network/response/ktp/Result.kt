package com.arifaradisa.spctest.data.network.response.ktp


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("ktp")
    val ktp: List<Ktp>
)