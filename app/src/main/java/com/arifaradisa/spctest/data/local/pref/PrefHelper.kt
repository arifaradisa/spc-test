package com.arifaradisa.spctest.data.local.pref

import android.content.Context
import android.content.SharedPreferences
import com.arifaradisa.spctest.util.AppConstant
import javax.inject.Inject

class PrefHelper @Inject constructor(context: Context) {

    val PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN"
    val PREF_KEY_USERNAME = "PREF_KEY_USERNAME"
    val PREF_KEY_LOGGED = "PREF_KEY_LOGGED"
    val sharedPref: SharedPreferences = context.getSharedPreferences(AppConstant.prefName, 0)

    fun clearData(){
        setAccessToken("")
        setUsername("")
        setLogged(false)
    }

    fun getAccessToken(): String? {
        return sharedPref.getString(PREF_KEY_ACCESS_TOKEN, "")
    }

    fun setAccessToken(accessToken: String) {
        sharedPref.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply()
    }

    fun getUsername(): String? {
        return sharedPref.getString(PREF_KEY_USERNAME, "")
    }

    fun setUsername(username: String) {
        sharedPref.edit().putString(PREF_KEY_USERNAME, username).apply()
    }

    fun isLogged(): Boolean? {
        return sharedPref.getBoolean(PREF_KEY_LOGGED, false)
    }

    fun setLogged(logged: Boolean) {
        sharedPref.edit().putBoolean(PREF_KEY_LOGGED, logged).apply()
    }

}