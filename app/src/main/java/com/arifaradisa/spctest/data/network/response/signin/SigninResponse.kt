package com.arifaradisa.spctest.data.network.response.signin


import com.google.gson.annotations.SerializedName

data class SigninResponse(
    @SerializedName("status")
    val status: Boolean,
    @SerializedName("status_code")
    val statusCode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("result")
    val result: Result
)