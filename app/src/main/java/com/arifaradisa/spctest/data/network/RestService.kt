package com.arifaradisa.spctest.data.network

import com.arifaradisa.spctest.data.network.response.add.AddResponse
import com.arifaradisa.spctest.data.network.response.ktp.ListResponse
import com.arifaradisa.spctest.data.network.response.signin.SigninResponse
import com.arifaradisa.spctest.data.network.response.signout.SignoutResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*
import javax.inject.Singleton

@Singleton
interface RestService {

    @FormUrlEncoded
    @POST("/v1/user/signin")
    suspend fun doSignin(
        @Field("username") username: String,
        @Field("password") password: String
    ): Response<SigninResponse>

    @GET("/v1/user/signout")
    suspend fun doSignout(
        @Header("x-access-token") token: String?
    ): Response<SignoutResponse>

    @GET("/v1/ktp/list")
    suspend fun getList(
        @Header("x-access-token") token: String?,
        @Query("page") page: Int
    ): Response<ListResponse>

    @Multipart
    @POST("/v1/ktp/add")
    suspend fun addKtp(
        @Header("x-access-token") token: String?,
        @Part photo: MultipartBody.Part,
        @Part("nik") nik: RequestBody,
        @Part("nama") nama: RequestBody,
        @Part("tmp_lahir") tmp_lahir: RequestBody,
        @Part("tgl_lahir") tgl_lahir: RequestBody,
        @Part("jenis_kelamin") jenis_kelamin: RequestBody,
        @Part("gol_darah") gol_darah: RequestBody,
        @Part("alamat") alamat: RequestBody,
        @Part("rt") rt: RequestBody,
        @Part("rw") rw: RequestBody,
        @Part("kelurahan") kelurahan: RequestBody,
        @Part("kecamatan") kecamatan: RequestBody,
        @Part("kabupaten") kabupaten: RequestBody,
        @Part("provinsi") provinsi: RequestBody,
        @Part("agama") agama: RequestBody,
        @Part("perkawinan") perkawinan: RequestBody,
        @Part("pekerjaan") pekerjaan: RequestBody,
        @Part("warga_negara") warga_negara: RequestBody,
        @Part("berlaku") berlaku: RequestBody,
        @Part("tmp_keluar") tmp_keluar: RequestBody,
        @Part("tgl_keluar") tgl_keluar: RequestBody,
        @Part("img_path") image_path: RequestBody
    ): Response<AddResponse>
}