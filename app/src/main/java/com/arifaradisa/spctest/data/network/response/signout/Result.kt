package com.arifaradisa.spctest.data.network.response.signout


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("message")
    val message: String
)