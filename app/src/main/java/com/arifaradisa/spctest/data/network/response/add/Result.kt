package com.arifaradisa.spctest.data.network.response.add


import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("message")
    val message: String
)