package com.arifaradisa.spctest.internal

enum class Status {
    SUCCESS, LOADING, ERROR
}